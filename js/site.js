/*********************Mobile menu********************************/
//Bind the menu for mobiles devices
$('.btn-navbar.btn').live("click",function(){
    $('.nav-collapse').toggleClass('collapse');
});

/*********************Geolocation*******************************/
//Activate the map
var map;

function initialize() {
    var myOptions = {
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('map'), myOptions);

    // Try HTML5 geolocation
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = new google.maps.LatLng(position.coords.latitude,
                position.coords.longitude);

            var infowindow = new google.maps.InfoWindow({
                map: map,
                position: pos,
                content: 'You are here'
            });

            map.setCenter(pos);
        },
            function() {
            handleNoGeolocation(true);
        });
    }
//    else {
//        Browser doesn't support Geolocation
//        handleNoGeolocation(false);
//    }
}

function handleNoGeolocation(errorFlag) {
    Modernizr.load({
        test: false,
        nope: "http://j.maxmind.com/app/geoip.js",
        complete: function(){
            var pos = new google.maps.LatLng(geoip_latitude(), geoip_longitude());

            var infowindow = new google.maps.InfoWindow({
                map: map,
                position: pos,
                content: 'You are here'
            });
            map.setCenter(pos);
        }
    });
//    if (errorFlag) {
//        var content = 'Error: The Geolocation service failed.';
//    } else {
//        var content = 'Error: Your browser doesn\'t support geolocation.';
//    }
//
//    var options = {
//        map: map,
//        position: new google.maps.LatLng(60, 105),
//        content: content
//    };
//
//    var infowindow = new google.maps.InfoWindow(options);
//    map.setCenter(options.position);
}

google.maps.event.addDomListener(window, 'load', initialize);

/*********************Local storage*******************************/
function loadStoredUserData(username){
    $('.userdata table tbody').empty()
        .append(
        $("<tr><td>"+(new Date()).toLocaleString()+"</td><td>"+username+"</td></tr>")
    );
}

function saveStoredUserData($usernameInput){
    if($usernameInput.size() <= 0) return;
    var username = $usernameInput.val();
    if(username.length === 0){
        alert('Enter your name');
        return;
    }
    localStorage["Adaptive:username"] = username;
    loadStoredUserData(username);
}

$("button[name='btn_save_username']").live("click", function(e){
    saveStoredUserData($(this).parent().find("input[name='username_input']"));
});

$("input[name='username_input']").live("keydown", function(e){
    if (e.which == 13) {
        e.preventDefault();
        saveStoredUserData($(this));
        $(this).blur();
    }
});

if(typeof(localStorage["Adaptive:username"]) !== "undefined"){
    loadStoredUserData(localStorage["Adaptive:username"]);
}

/*********************History API*******************************/
//Put an article in "full screen"
$(".icons .expand").live("click", function(e){
    if($("#expandedArticle").size()>0){
        $("#expandedArticle").remove();
    }
    var $article = $(this).parents('article');
    var $mainContainer = $('.container[role=main]');
    var width$mainContainer = $mainContainer.width();
    var height$mainContainer = $mainContainer.height();
    var offset$mainContainer = $mainContainer.offset();
    var top$mainContainer = offset$mainContainer.top;
    var left$mainContainer = offset$mainContainer.left;

    var $expandedArticle = $article.clone(true);

    $expandedArticle.css({
        top: top$mainContainer,
        left: left$mainContainer,
        position: "absolute",
        "z-index": 1000,
        width: width$mainContainer,
        "background-color": "white",
        height: height$mainContainer
    }).attr({id: "expandedArticle"});

    $("article").hide();

    //Should be generalised
    var $corpse = $expandedArticle.find(".corpse");
    $corpse.css("height", "auto");
    var corpseExtraCssData = $corpse.data();
    for(var cssKey in corpseExtraCssData){
        $corpse.css(cssKey, corpseExtraCssData[cssKey]);
    }
    //END-Should be generalised

    $expandedArticle.find(".icons .expand").die().text("Ö").addClass("close");
    $expandedArticle.find(".icons .expand.close").click(function(evt){
        $("#expandedArticle").remove();
        $("article").show();
        history.pushState(null, null, "/adaptive/");
        return false;
    });

    $expandedArticle.appendTo($mainContainer);
    history.pushState(null, null, $expandedArticle.data("url"));
});

//This part is hardcoded here which is not great but that's not the purpose of the demo
function managesArticleExpansion(){
    //History.emulated.pushState (==true for oldBrowsers)
    var location = (typeof(History) !== 'undefined' && typeof(History.emulated) !== 'undefined' && History.emulated.pushState) ? document.location.hash : document.location.pathname;
    var pathname = location.split('/');
    var endPath = pathname.pop();
    if(endPath.length === 0){
        //Closing previous opened article if url is /
        $(".icons span.close").click();
    }
    else if($(".icons span.close","[data-url='"+endPath+"']").size() == 0){
        $(".icons span","[data-url='"+endPath+"']").click();
    }
}


$(document).ready(function(){
    history.replaceState(null, null, document.location.href);
    managesArticleExpansion();
});

//window.addEventListener('popstate', function(event) {
$(window).bind('popstate', function(event) {
    console.log("popstate");
    managesArticleExpansion();
});


/*********************Utils*******************************/
/* Normalized hide address bar for iOS & Android (c) Scott Jehl, scottjehl.com MIT License */
(function(a){var b=a.document;if(!location.hash&&a.addEventListener){window.scrollTo(0,1);var c=1,d=function(){return a.pageYOffset||b.compatMode==="CSS1Compat"&&b.documentElement.scrollTop||b.body.scrollTop||0},e=setInterval(function(){if(b.body){clearInterval(e);c=d();a.scrollTo(0,c===1?0:1)}},15);a.addEventListener("load",function(){setTimeout(function(){if(d()<20){a.scrollTo(0,c===1?0:1)}},0)})}})(this);

function reloadWindow(w){
    w.location.href = w.location.href;
}

/*********************Setters for current users specificities*******************************/
//Colorblind
function setColorBlind(){
    setCookie("colorblind", true);
    setCookie("lowvision", false);
    setCookie("blind", false);
}

//Low vision
function setLowVision(){
    setCookie("lowvision", true);
    setCookie("colorblind", false);
    setCookie("blind", false);
}

//Blind
function setBlind(){
    setCookie("blind", true);
    setCookie("colorblind", false);
    setCookie("lowvision", false);
}

//No disabilities (unset others
function setNoDisabilites(){
    setCookie("colorblind", false);
    setCookie("lowvision", false);
    setCookie("blind", false);
}

