//======================================================================================================================
// Common polyfills
//======================================================================================================================
// usage: log('inside coolFunc', this, arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function f(){ log.history = log.history || []; log.history.push(arguments); if(this.console) { var args = arguments, newarr; args.callee = args.callee.caller; newarr = [].slice.call(args); if (typeof console.log === 'object') log.apply.call(console.log, console, newarr); else console.log.apply(console, newarr);}};

// make it safe to use console.log always
(function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
(function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());

var DEBUG = true;
var debug = function(){
    if(DEBUG){
        try{
            Function.prototype.apply.call(console.log, null, arguments);//IE
        }
        catch(err){
            console.log.apply(console, arguments);//Other
        }
    }
}

if (!Object.keys) {
    Object.keys = (function () {
        var hasOwnProperty = Object.prototype.hasOwnProperty,
            hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
            dontEnums = [
                'toString',
                'toLocaleString',
                'valueOf',
                'hasOwnProperty',
                'isPrototypeOf',
                'propertyIsEnumerable',
                'constructor'
            ],
            dontEnumsLength = dontEnums.length

        return function (obj) {
            if (typeof obj !== 'object' && typeof obj !== 'function' || obj === null) throw new TypeError('Object.keys called on non-object')

            var result = [];

            for (var prop in obj) {
                if (hasOwnProperty.call(obj, prop)) result.push(prop)
            }

            if (hasDontEnumBug) {
                for (var i=0; i < dontEnumsLength; i++) {
                    if (hasOwnProperty.call(obj, dontEnums[i])) result.push(dontEnums[i])
                }
            }
            return result;
        }
    })()
};

//Cookies helpers (http://www.w3schools.com/js/js_cookies.asp)
function setCookie(c_name,value,exdays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name)
{
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==c_name)
        {
            return unescape(y);
        }
    }
}

//======================================================================================================================
// Adaptive.js
// Author: R.BECK @rphbck
// Date: 06/2012
//======================================================================================================================
var containsDotRE = new RegExp("\\.");

/**
 * Base for plugins modules of Adaptative
 * @constructor
 */
var Module = function(moduleName){
    "use strict";
    this._detections = {};
    this._adaptations = {};
    this._name = moduleName;
};

/**
 * Getter / Setter for detection methods
 * @param key Key of the detection method
 * @param callback Function of the detection method
 * @return A function (when getter) or undefined (when setter)
 */
Module.prototype.detection = function(key, callback){
    "use strict";
    var length = arguments.length;

    if(length < 1){
        throw new Error("key argument is mandatory");
    }
    else if(String(key).match(containsDotRE)){
        throw new Error("dot character is not allowed in keys");
    }
    else if(length === 1){//Getter
        return this._detections[key];
    }
    else{//Setter
        if(typeof(callback) !== "function"){
            throw new Error("callback should be a callable");
        }
        this._detections[key] = callback;
    }
};

/**
 * Getter / Setter for adaptation methods
 * @param key Key of the adaptation method
 * @param callback Function of the adaptation method
 * @return A function (when getter) or undefined (when setter)
 */
Module.prototype.adaptation = function(key, callback){
    "use strict";
    var length = arguments.length;

    if(length < 1){
        throw new Error("key argument is mandatory");
    }
    else if(String(key).match(containsDotRE)){
        throw new Error("dot character is not allowed in keys");
    }
    else if(length === 1){//Getter
        return this._adaptations[key];
    }
    else{//Setter
        if(typeof(callback) !== "function"){
            throw new Error("callback should be a callable");
        }
        this._adaptations[key] = callback;
    }
};

/**
 * Get all detections keys
 * @return {Array} Returns all detections keys
 */
Module.prototype.getDetections = function(){
    "use strict";
    //Object.keys is not supported in IE6 to 8, FF3.5-3.6, SF4, OP10.5-11.5, Konq 4.3
    //So, in these browsers this has to be polyfilled.
    //A proposed implementation can be found here http://whattheheadsaid.com/2010/10/a-safer-object-keys-compatibility-implementation
    //or here http://stackoverflow.com/questions/208016/how-to-list-the-properties-of-a-javascript-object/3937321
    return Object.keys(this._detections);
};

/**
 * Get all adaptations keys
 * @return {Array} Returns all adaptations keys
 */
Module.prototype.getAdaptations = function(){
    "use strict";
    return Object.keys(this._adaptations);//See comment in getDetections about Object.keys
};

/**
 * Launch every registered detection method and return which ones succeeded
 * @return {Array} of string of succeeded detection methods
 */
Module.prototype.detect = function(){
    "use strict";
    //TODO: Maybe add a parameter object with extra arguments for detections methods
    //I.e: {'key1': [1, function(){}, ...]}

    var detected = [],
        _detections = this._detections;
    for(var k in _detections){
        if(_detections[k]()){
            detected.push(k);
        }
    }
    return detected;
};

/**
 * Applies adaptations for keys.
 * If the key hasn't be registered fail silently
 * @param keys {string | array}
 */
Module.prototype.adapt = function(keys){
    "use strict";
    if(typeof(keys) === "string"){
        keys = [keys];
    }

    var _adaptations = this._adaptations;

    for(var k in keys){
        if(keys[k] in _adaptations){
            _adaptations[keys[k]]();
        }
    }
};

/**
 * Manages adaptations for users
 * @constructor
 */
var User = function(){};
User.prototype = new Module("user");

/**
 * Manages adaptations for devices
 * @constructor
 */
var Device = function(){};
Device.prototype = new Module("device");

/**
 * Manages adaptations for browsers features
 * @constructor
 */
var Features = function(){};
Features.prototype = new Module("features");

(function(window){
    "use strict";
    var Adaptive = function(){
        var _modules = {};

        /**
         * Tells if a module is registered for the name
         * @param moduleName Name of the module
         * @return {Boolean} True if a module is registered with this name, false otherwise
         */
        this.hasModule = function(moduleName){
            return (moduleName in _modules);
        };

        /**
         * Get all names of registered modules
         * @return {Array} Names of registered modules
         */
        this.getModules = function(){
            //Object.keys does not return the right type but a string representation for each key
            return Object.keys(_modules);
        };

        /**
         * Register a module.
         * @param module Module to register. Has to implement Module & set a _name
         */
        this.registerModule = function(module){
            if(!(module instanceof Module) || !module._name){
                throw new Error("module has to be a subclass of module");
            }
            var module_name = module._name;
            if(String(module_name).match(containsDotRE)){
                throw new Error("dot character is not allowed in name of the module");
            }
            _modules[module_name] = module;
        };

        /**
         * Register an extra detection method for a particular module.
         * The module has to be already registered
         * The callback has to be callable & return a boolean which indicates whether or not the detection has succeeded
         * @param moduleName Name of a previously registered module
         * @param detectionName Name of the detection. NB: If the detection key already exists it will be overwritten
         * @param detectionCallback Detection callable
         */
        this.registerDetectionForModule = function(moduleName, detectionName, detectionCallback){
            if(arguments.length < 3){
                throw new Error("All arguments are mandatory");
            }

            if(!this.hasModule(moduleName)){
                throw new Error("Module not registered");
            }

            if(typeof(detectionCallback) !== "function"){
                throw new Error("detectionCallback has to be callable");
            }

            _modules[moduleName].detection(detectionName, detectionCallback);
        };

        /**
         * Launch detect for every registered module
         * @return {Array of string} All names of detections
         */
        this.detect = function(){
            var detected = [];
            for(var moduleName in _modules){
                var moduleDetected = _modules[moduleName].detect();
                for(var i = 0, l=moduleDetected.length; i < l; ){
                    detected.push(moduleName+"."+moduleDetected[i++]);
                }
            }
            return detected;
        };

        /**
         * Launch adaptations for provided keys
         * @param keys A string composed of the name of a registered module and the name of an adaptation within this
         *             module separated by a dot
         */
        this.adapt = function(keys){
            if(typeof(keys) === "string"){
                keys = [keys];
            }

            for(var k in keys){
                try{
                    var moduleAdaptionName = keys[k].split(".");
                    var module = moduleAdaptionName[0];
                    var adaptationName = moduleAdaptionName[1];
                    _modules[module].adaptation(adaptationName)(window);
                } catch(err){
                    //Fail silently
                    console.log("---CATCH---");
                    console.log(keys[k]);
                    console.log(err);
                    console.log("---END CATCH---");
                }
            }
        };

        /**
         * Register an extra adaptation method for a particular module.
         * The module has to be already registered
         * The callback has to be callable & return a boolean which indicates whether or not the detection has succeeded
         * @param moduleName Name of a previously registered module
         * @param adaptationName Name of the adaptation. NB: If the adaptation key already exists it will be overwritten
         * @param adaptationCallback Adaptation callable
         */
        this.registerAdaptationForModule = function(moduleName, adaptationName, adaptationCallback){
            if(arguments.length < 3){
                throw new Error("All arguments are mandatory");
            }
            if(!this.hasModule(moduleName)){
                throw new Error("Module not registered");
            }
            if(typeof(adaptationCallback) !== "function"){
                throw new Error("detectionCallback has to be callable");
            }
            _modules[moduleName].adaptation(adaptationName, adaptationCallback);
        };

        /**
         * Remove all registered modules
         */
        this.reset = function(){
            _modules = {};
        };

    };

    window.Adaptive = new Adaptive();
})(window);

//======================================================================================================================
// Adaptive plugins
//======================================================================================================================
(function(window){//Keep modules private
    //================= Registration of three base plugins
    //Device
    var d = new Device();
    Adaptive.registerModule(d);

    //User
    var u = new User();
    Adaptive.registerModule(u);

    //Features
    var f = new Features();
    Adaptive.registerModule(f);

    /*Util for the demo*/
    function tellAdaptation(msg){
        $("article.adaptations .corpse").append($("<p>"+msg+"</p>"));
    }

    //================= Registration of polyfills

    //Media queries polyfills
    d.detection("mediaqueries_api", function(){
        debug("Detect:mediaqueries_api:"+(!Modernizr.mq('only screen')));
        //The media query "screen" is valid for desktop/tablet/mobile
        //So if Media queries are not supported Modernizr.mq('screen') will return false
        //Then the detection method has to return true in order to tell the feature
        //has to be polyfilled
        return !Modernizr.mq('only screen');
    });

    d.adaptation("mediaqueries_api", function(){
        debug("Adapt:mediaqueries_api");
        $("article.adaptations .corpse").append($("<p>"+"Adapt:media queries API"+"</p>"));
        tellAdaptation("Adapt:media queries API");
        Modernizr.load({
            test: !navigator.userAgent.match('MSIE 8.0'),
            nope: [
                "css/mobile.css",
                "css/tablet.css",
                "css/desktop.css",
                "js/libs/respond.min.js"
            ],
            complete: function(){

            }
        });
    });

    d.detection("mq_mobile", function(){
        debug("Detect:mq_mobile:"+(Modernizr.mq('only screen and (min-width: 320px)')));
        return Modernizr.mq('only screen and (min-width: 320px)');
    });

    d.adaptation("mq_mobile", function(){
        debug("Adapt:mq_mobile");
        //TODO: Load on demand Harvey
        tellAdaptation("Adapt:media queries for mobile");
        Harvey.attach('only screen and (min-width: 320px)', {
            on: function(){
                Modernizr.load("css/mobile.css");
            }
            //    setup: function(){Modernizr.load("css/test1.css");},
            //    off: function(){$('link[href="css/test1.css"]').remove();}
        });
    });

    d.detection("mq_tablet", function(){
        debug("Detect:mq_tablet:"+(Modernizr.mq('only screen and (min-width: 768px)')));
        return Modernizr.mq('only screen and (min-width: 768px)');
    });

    d.adaptation("mq_tablet", function(){
        debug("Adapt:mq_tablet");
        tellAdaptation("Adapt:media queries for tablet");
        //TODO: Load on demand Harvey
        Harvey.attach('only screen and (min-width: 768px)', {
            on: function(){
                Modernizr.load("css/tablet.css");
            }
        });
    });

    d.detection("mq_desktop", function(){
        debug("Detect:mq_desktop:"+(Modernizr.mq('only screen and (min-width: 1140px)')));
        return Modernizr.mq('only screen and (min-width: 1140px)');
    });

    d.adaptation("mq_desktop", function(){
        debug("Adapt:mq_desktop");
        tellAdaptation("Adapt:media queries for desktop");
        //TODO: Load on demand Harvey
        Harvey.attach('only screen and (min-width: 1140px)', {
            on: function(){
                Modernizr.load("css/desktop.css");
            }
        });
    });

    f.detection("geolocation", function(){
        console.log("Detect:geolocation:"+(!navigator.geolocation));
        return !navigator.geolocation;
    });

    f.adaptation("geolocation", function(w){
        console.log("Adapt:geolocation");
        tellAdaptation("Adapt:geolocation");
        Modernizr.load({
            test: Modernizr.geolocation,
            nope: "http://j.maxmind.com/app/geoip.js",
            complete: function(){
                var pos = new google.maps.LatLng(geoip_latitude(), geoip_longitude());

                if(!map || typeof(map) === "undefined"){
                    var myOptions = {
                        zoom: 6,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(document.getElementById('map'), myOptions);
                }

                var infowindow = new google.maps.InfoWindow({
                    map: map,
                    position: pos,
                    content: 'You are here'
                });
                map.setCenter(pos);
            }
        });

    });

    f.detection("picturefill", function(){
        console.log("Detect:picturefill:"+((typeof(picturefill) === "undefined")));
        return (typeof(picturefill) === "undefined");
    });

    f.adaptation("picturefill", function(w){
        tellAdaptation("Adapt:picturefill");
        Modernizr.load([
            {
                test: !!window.matchMedia,
                nope: "js/libs/matchmedia.js"
            },
            {
                test: false,
                nope: "js/libs/picturefill.js",
                complete: function(){
                    picturefill();
                }
            }
        ]);
    });

    f.detection("localStorage", function(){
        console.log("Detect:localStorage:"+(typeof window.localStorage == 'undefined'));
        return (typeof window.localStorage === 'undefined');
    });

    f.adaptation("localStorage", function(w){
        console.log("Adapt:localStorage");
        tellAdaptation("Adapt:localStorage");
        Modernizr.load("js/libs/storage.js");
    });

    f.detection("history", function(){
        console.log("Detect:history:"+(typeof history.pushState === 'undefined'));
        return (typeof history.pushState === 'undefined');
    });

    f.adaptation("history", function(w){
        console.log("Adapt:history");
        tellAdaptation("Adapt:history");
        Modernizr.load(
            {
                test: (typeof(history.pushState) !== 'undefined'),//Could be false because already detected
                nope: "js/libs/jquery.history.js",
                complete: function(){
                    window.history.pushState = History.pushState;
                    window.history.replaceState = History.replaceState;
                    console.log("History loaded");
                }
        });
    });

    u.detection("colorblind", function(){
        var colorBlind = getCookie("colorblind");
        console.log("Detect:colorblind:"+((typeof colorBlind !== 'undefined') && colorBlind === "true"));
        return (typeof colorBlind !== 'undefined') && colorBlind === "true";
    });

    u.adaptation("colorblind", function(w){
        console.log("Adapt:colorblind");
        tellAdaptation("Adapt:colorblind");
        //Add tips for clickables images
        $('img').each(function(){
            var tips = $(this).data("tips");
            if(typeof tips !== 'undefined'){
                $(this).after($("<p></p>").text(tips));
            }
            //Raise the contrast of images
            $(this).css("-webkit-filter", "contrast(200%)");
        });
    });

    u.detection("lowvision", function(){
        var lowVision = getCookie("lowvision");
        console.log("Detect:lowvision:"+((typeof lowVision !== 'undefined') && lowVision === "true"));
        return (typeof lowVision !== 'undefined') && lowVision === "true";
    });

    u.adaptation("lowvision", function(w){
        console.log("Adapt:lowvision");
        tellAdaptation("Adapt:lowvision");
        Modernizr.load(
            {
                test: false,
                nope: "css/lowvision.css",
                complete: function(){
                    console.log("Low vision complete");
                }
        });
    });

    u.detection("blind", function(){
        var blind = getCookie("blind");
        console.log("Detect:blind:"+((typeof blind !== 'undefined') && blind === "true"));
        return (typeof blind !== 'undefined') && blind === "true";
    });

    u.adaptation("blind", function(w){
        console.log("Adapt:blind");
        tellAdaptation("Adapt:blind");

        var tabIndexCandidates = ["[role='menu']", "[role='menuitem']", "article", "a[role!='menuitem']"];
        for(var i=0, l=tabIndexCandidates.length; i < l; i++){
            var $nodes = $(tabIndexCandidates[i]);
            $nodes.attr("tabindex", i);
        }
        Modernizr.load("css/blind.css");
    });

})(window);