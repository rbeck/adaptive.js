/**********************************************************************************************************************/

$(document).ready(function(){
    //Loading of qunit script
    //Modernizr.load("tests/qunit/qunit.js");

    //Loading of qunit style
    Modernizr.load("tests/qunit/qunit.css");

    //Loading of tests style
    Modernizr.load("tests/tests.css");

    var isMobile = false, isTablet = false, isDesktop = false;
    var UA = navigator.userAgent;
    var isIE = UA.match('MSIE 8.0');
    //My testing platform
    //Chrome 19
    //Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5
    //IE 8
    //Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; InfoPath.2; .NET4.0E)
    //Safari on iPhone 5.0.1

    //Android tablet ICS


    //Very dummy detection for tests
    if(isIE || UA.match('Chrome')){
        isDesktop = true;
    } else if(UA.match('iPhone')){
        isMobile = true;
    } else if(UA.match('Android')){
        isTablet = true;
    }

    module("Tests");
    test('Base plugins', function() {
        deepEqual(Adaptive.getModules(), ["device", "user", "features"], "Device, User, Features base plugins should be registered");
    });

    asyncTest('Loading of right css', function() {
        Adaptive.adapt(Adaptive.detect());

        setTimeout(function(){
        var mobileCss = document.querySelector("link[href='css/mobile.css']");
        var tabletCss = document.querySelector("link[href='css/tablet.css']");
        var desktopCss = document.querySelector("link[href='css/desktop.css']");

        ok(mobileCss, 'Mobile css well loaded');
        if(isDesktop){
            ok(desktopCss, 'Desktop css well loaded');
        }
        if(isDesktop || isTablet){
            ok(tabletCss, 'Tablet css well loaded');
        }
        start();
        }, 3000);
    });

    asyncTest('Geolocation API', function() {
        Adaptive.adapt(Adaptive.detect());

        setTimeout(function(){
            ok(true, "Fake test");
            if(isIE){
                ok(Adaptive.detect().indexOf("features.geolocation") > -1, "Geolocation is well detected as not existing");
            }
            start();
        }, 3000);
    });

});







