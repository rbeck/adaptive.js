/*******************************************Utils section for tests****************************************************/
/**
 * Check if two arrays are deeply equals
 * @param arr1 An array
 * @param arr2 An array
 * @return {Boolean} True if both array are equals, false otherwise
 * Code taken from http://stackoverflow.com/questions/4025893/how-to-check-identical-array-in-most-efficient-way
 */
function arraysEqual(arr1, arr2) {
    if(arr1.length !== arr2.length)
        return false;
    for(var i = arr1.length; i--;) {
        if(arr1[i] !== arr2[i])
            return false;
    }

    return true;
}

/**
 * Demo code for "Extreme JavaScript Performance" @ JSConf.eu
 * @param m Function to benchmark
 * @param i Number of times
 * //benchmark(function(){
 * //  parseInt(str);
 * //}, 1000000);
 */
var benchmark = function(m,i){
    var d1 = new Date, d2, r;
    while(i--) r = m();
    d2 = new Date;
    return (d2.getTime()-d1.getTime());
    // if(r) console.log(r);
};

if(!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(needle) {
        for(var i = 0; i < this.length; i++) {
            if(this[i] === needle) {
                return i;
            }
        }
        return -1;
    };
}