/**********************************************************************************************************************/

$(document).ready(function(){
//    $('<div class="overlay"><div><h1 id="qunit-header">Adaptive.js tests</h1><h2 id="qunit-banner"></h2><h2 id="qunit-userAgent"></h2><p id="qunit-testresult" class="result"></p><ol id="qunit-tests"></ol></div></div>').appendTo($(document.body));

    //Loading of qunit script
    //Modernizr.load("tests/qunit/qunit.js");

    //Loading of qunit style
    Modernizr.load("tests/qunit/qunit.css");

    //Loading of tests style
    Modernizr.load("tests/tests.css");

    //To be sure that existing plugins are not conflicting with tests we flush the framework
    Adaptive.reset();

    module("Tests 'Module'");

    test('Properties', function() {
        var m = new Module();
        ok(m._detections instanceof Object, '_detections is present and is an Object');
        ok(m._adaptations instanceof Object, '_adaptations is present and is an Object');
        equal(m._name, undefined, 'Not providing a name let it undefined');

        var moduleName = "MyModule";
        var m2 = new Module(moduleName);
        equal(m2._name, moduleName, 'Name of Module is set');
    });

    test('Methods: detection', function(){
        var m = new Module();
        ok(m.detection instanceof Function, 'detection is a method');
        raises(function(){m.detection();}, Error, 'First parameter is mandatory, so supposed to raise an Error');

        deepEqual(m.detection(""), undefined, 'Nothing registered yet, so nothing to get.');

        //Set a detection callback
        var detectKey = "detectionMethod";
        m.detection(detectKey, function(){return true;});
        ok(typeof(m._detections[detectKey]) != "undefined", "Check if the function has been set well");
        notEqual(m._detections[detectKey], undefined, "Check if the function has been set well");


        raises(function(){m.detection(detectKey, null);}, Error, 'callback should be a callable');
        raises(function(){m.detection(detectKey, undefined);}, Error, 'callback should be a callable');
        raises(function(){m.detection(detectKey, 1);}, Error, 'callback should be a callable');
        raises(function(){m.detection(detectKey, "");}, Error, 'callback should be a callable');

        //Get a detection callback
        ok(m.detection(detectKey) instanceof Function, "Check if it's a function");
        ok(m.detection(detectKey)(), "Check if it's the right function");

        raises(function(){m.detection("a.b", function(){return true;});}, Error, 'Dot is not accepted');
        raises(function(){m.detection("a..b", function(){return true;});}, Error, 'Dot is not accepted');
        raises(function(){m.detection(".b", function(){return true;});}, Error, 'Dot is not accepted');
        raises(function(){m.detection("b.", function(){return true;});}, Error, 'Dot is not accepted');
        raises(function(){m.detection("b...........", function(){return true;});}, Error, 'Dot is not accepted');

    });

    test('Methods: adaptation', function(){
        var m = new Module();
        ok(m.adaptation instanceof Function, 'adaptation is a method');
        raises(function(){m.adaptation();}, Error, 'First parameter is mandatory, so supposed to raise an Error');

        deepEqual(m.adaptation(""), undefined, 'Nothing registered yet, so nothing to get.');

        //Set an adaptation callback
        var adaptKey = "adapt1";
        m.adaptation(adaptKey, function(){return true;});
        ok(typeof(m._adaptations[adaptKey]) != "undefined", "Check if the function has been set well");
        notEqual(m._adaptations[adaptKey], undefined, "Check if the function has been set well");

        raises(function(){m.adaptation(adaptKey, null);}, Error, 'callback should be a callable');
        raises(function(){m.adaptation(adaptKey, undefined);}, Error, 'callback should be a callable');
        raises(function(){m.adaptation(adaptKey, 1);}, Error, 'callback should be a callable');
        raises(function(){m.adaptation(adaptKey, "");}, Error, 'callback should be a callable');

        //Get a detection callback
        ok(m.adaptation(adaptKey) instanceof Function, "Check if it's a function");
        ok(m.adaptation(adaptKey)(), "Check if it's the right function");

        raises(function(){m.adaptation("a.b", function(){return true;});}, Error, 'Dot is not accepted');
        raises(function(){m.adaptation("a..b", function(){return true;});}, Error, 'Dot is not accepted');
        raises(function(){m.adaptation(".b", function(){return true;});}, Error, 'Dot is not accepted');
        raises(function(){m.adaptation("b.", function(){return true;});}, Error, 'Dot is not accepted');
        raises(function(){m.adaptation("b...........", function(){return true;});}, Error, 'Dot is not accepted');
    });

    test('Methods: getDetections', function(){
        var m = new Module();
        ok(m.getDetections instanceof Function, 'getDetections is a method');
        deepEqual(m.getDetections(), [], 'Nothing registered');

        var cb = function(){return true};
        var key1 = "1", key2 = "2", key3= "3";
        var result = [key1, key2, key3];

        m.detection(key1, cb);
        m.detection(key2, cb);
        m.detection(key3, cb);

//        ok(arraysEqual(m.getDetections(), result), "Returned detections are those ones registered");
        deepEqual(m.getDetections(), result, "Returned detections are those ones registered");

    });

    test('Methods: getAdaptations', function(){
        var m = new Module();
        ok(m.getAdaptations instanceof Function, 'getAdaptations is a method');
        deepEqual(m.getAdaptations(), [], 'Nothing registered');

        var cb = function(){return true};
        var key1 = "1", key2 = "2", key3= "3";
        var result = [key1, key2, key3];

        m.adaptation(key1, cb);
        m.adaptation(key2, cb);
        m.adaptation(key3, cb);

        ok(arraysEqual(m.getAdaptations(), result), "Returned adaptations are those ones registered");

    });

    test('Methods: detect', function(){
        var m = new Module();
        ok(m.detect instanceof Function, 'detect is a method');
        deepEqual(m.detect(), [], 'Nothing registered, so nothing has been detected');

        var cb  = function(){return true};
        var cb2 = function(){return false};
        var key1 = "1", key2 = "2", key3= "3", key4 = "4";

        m.detection(key1, cb);
        m.detection(key2, cb2);
        m.detection(key3, cb);

        var result = [key1, key3];

//        ok(arraysEqual(m.detect(), result), "Things actually detected");
        deepEqual(m.detect(), result, "Things actually detected");

        var m2 = new Module();
        var m2_cb = function(){return (window) ? true : false;}
        var m2_cb2 = function(){return (window.document) ? true : false;}
        var m2_cb3 = function(){return Modernizr.mq('only screen');}

        m2.detection(key1, m2_cb);
        m2.detection(key2, m2_cb2);
        m2.detection(key3, m2_cb3);

        var result2 = [key1, key2, key3];
//        ok(arraysEqual(m2.detect(), result), "Things actually detected");
        deepEqual(m2.detect(), result2, "Things actually detected");

    });

    module("Tests 'Module'", {
        setup: function(){
            var d = document.createElement('div');
            d.setAttribute("id", "test-module-adapt1");
            var d2 = document.createElement('div');
            d2.setAttribute("id", "test-module-adapt2");
            d.appendChild(d2);
            document.body.appendChild(d);
            this.div = d;
            this.div2 = d2;
            equal(this.div.firstChild, this.div2, 'Div2 is a child of Div');
        },
        teardown: function(){
            this.div2.parentNode.removeChild(this.div2);
            this.div.parentNode.removeChild(this.div);
            delete this.div;
            delete this.div2;
        }}
    );

    test('Methods: adapt', function(){
        expect(7);
        var m = new Module();
        var _this = this;
        var key1 = "1", key2 = "2", key3= "3", key4 = "4";

        ok(m.adapt instanceof Function, 'adapt is a method');


        var cb1 = function(){
            _this.div.style.backgroundColor = "rgb(0, 0, 0)";
        };

        var cb2 = function(){
            _this.div.style.borderColor = "#000000";
            _this.div.setAttribute("data-test", 12345);
        };

        var cb3 = function(){
            _this.div2 = _this.div.removeChild(_this.div2);
            document.body.appendChild(_this.div2);
        };

        m.adaptation(key1, cb1);
        m.adaptation(key2, cb2);
        m.adaptation(key3, cb3);

        m.adapt(key1);
        equal(_this.div.style.backgroundColor, "rgb(0, 0, 0)", 'Adaptation 1 well applied');

        m.adapt([key2, key3]);
        equal(_this.div.style.borderColor, "rgb(0, 0, 0)", 'Adaptation 2.1 well applied');

        equal(_this.div.getAttribute("data-test"), 12345, 'Adaptation 2.2 well applied');

        notEqual(_this.div.firstChild, _this.div2, 'Adaptation 3.1 well applied');
//        ok($('body > #test-module-adapt2'), 'Adaptation 3.2 well applied');
        equal(_this.div2.parentNode, document.body, 'Adaptation 3.2 well applied');

        m.adapt("invalid key");

    });

    module("Tests 'User'");
    test('Implements Module', function() {
        var u = new User();
        ok(u instanceof Module, 'User implements Module');
        ok(u._detections instanceof Object, '_detections is present and is an Object');
        ok(u._adaptations instanceof Object, '_adaptations is present and is an Object');

        equal(u._name, "user", 'Name of User is always "user"');
    });

    module("Tests 'Device'");
    test('Implements Module', function() {
        var d = new Device();
        ok(d instanceof Module, 'Device implements Module');
        ok(d._detections instanceof Object, '_detections is present and is an Object');
        ok(d._adaptations instanceof Object, '_adaptations is present and is an Object');

        equal(d._name, "device", 'Name of Device is always "device"');
    });

    module("Tests 'Features'");
    test('Implements Module', function() {
        var f = new Features();
        ok(f instanceof Module, 'Features implements Module');
        ok(f._detections instanceof Object, '_detections is present and is an Object');
        ok(f._adaptations instanceof Object, '_adaptations is present and is an Object');

        equal(f._name, "features", 'Name of Features is always "features"');
    });

    module("Tests 'Adaptive'");
    test('Adaptive: hasModule', function(){
        ok(Adaptive.hasModule instanceof Function, 'hasModule is a method');
        equal(Adaptive.hasModule(""), false, 'Nothing registered yet');
        equal(Adaptive.hasModule(null), false, 'Nothing registered yet');
        equal(Adaptive.hasModule(Module), false, 'Nothing registered yet');
        equal(Adaptive.hasModule(User), false, 'Nothing registered yet');
        equal(Adaptive.hasModule(undefined), false, 'Nothing registered yet');
        equal(Adaptive.hasModule(new User()), false, 'Nothing registered yet');
    });

    test('Adaptive: getModules', function(){
        ok(Adaptive.getModules instanceof Function, 'getModules is a method');
        deepEqual(Adaptive.getModules(), [], 'Nothing registered yet');
    });

    test('Adaptive: registerModule', function(){
        equal(Adaptive._modules, undefined, 'Should be private');
        ok(Adaptive.registerModule instanceof Function, 'registerModule is a method');

        raises(function(){Adaptive.registerModule(null);}, Error, 'module has to be a subclass of Module');
        raises(function(){Adaptive.registerModule(undefined);}, Error, 'module has to be a subclass of Module');
        raises(function(){Adaptive.registerModule(1);}, Error, 'module has to be a subclass of Module');
        raises(function(){Adaptive.registerModule(function(){});}, Error, 'module has to be a subclass of Module');
        raises(function(){Adaptive.registerModule(Module);}, Error, 'module has to be a subclass of Module');
        raises(function(){Adaptive.registerModule(User);}, Error, 'module has to be a subclass of Module');
        raises(function(){Adaptive.registerModule(new Module());}, Error, 'module has to be a subclass of Module and have to have a _name');

        var TestModule = function(){};
        TestModule.prototype = new Module("mod.ule");
        raises(function(){Adaptive.registerModule(new TestModule());}, Error, 'name of the module should not have a dot');

        var TestModule2 = function(){};
        TestModule2.prototype = new Module(2);//Name is not necessary a string. Have to check this!
        Adaptive.registerModule(new TestModule2());

        var TestModule3 = function(){};
        TestModule3.prototype = new Module(2.5);
        raises(function(){Adaptive.registerModule(new TestModule3());}, Error, 'name of the module should not have a dot');

        var u = new User();
        var d = new Device();
        var f = new Features();

        Adaptive.registerModule(u);
        ok(Adaptive.hasModule(u._name));
        deepEqual(Adaptive.getModules(), ["2", u._name], 'User() module has been well registered');

        ok(!Adaptive.hasModule(d._name), 'Not registered yet');

        Adaptive.registerModule(d);
        ok(Adaptive.hasModule(d._name));
        deepEqual(Adaptive.getModules(), ["2", u._name, d._name], 'User() & Device() module have been well registered');

        ok(!Adaptive.hasModule(f._name), 'Not registered yet');

        Adaptive.registerModule(f);
        ok(Adaptive.hasModule(f._name));
        deepEqual(Adaptive.getModules(), ["2", u._name, d._name, f._name], 'User() & Device() & Features() modules have been well registered');
    });

    test('Adaptive: reset', function(){
        ok(Adaptive.reset instanceof Function, 'reset is a method');
        Adaptive.reset();
        deepEqual(Adaptive.getModules(), [], "Nothing should be registered");
    });

    test('Adaptive: detect', function(){
        ok(Adaptive.detect instanceof Function, 'detect is a method');
        deepEqual(Adaptive.detect(), [], 'nothing registered');
    });

    test('Adaptive: registerDetectionForModule', function(){
        expect(16);
        ok(Adaptive.registerDetectionForModule instanceof Function, 'registerDetectionForModule is a method');
        var u = new User();
        var key1 = "1", key2 = "2", key3= "3", key4 = "4";
        Adaptive.registerModule(u);

        raises(function(){Adaptive.registerDetectionForModule(null, null, null);}, Error,  'module not registered');
        raises(function(){Adaptive.registerDetectionForModule(undefined, undefined, undefined);}, Error,  'module not registered');
        raises(function(){Adaptive.registerDetectionForModule("", "", "");}, Error,  'module not registered');

        Adaptive.registerDetectionForModule(u._name, key1, function(){});

        raises(function(){Adaptive.registerDetectionForModule(u._name, key1, null);}, Error,  'module registered, callback has to be callable');
        raises(function(){Adaptive.registerDetectionForModule(u._name, key1, "");}, Error,  'module registered, callback has to be callable');
        raises(function(){Adaptive.registerDetectionForModule(u._name, key1, undefined);}, Error,  'module registered, callback has to be callable');
        raises(function(){Adaptive.registerDetectionForModule(u._name, key1, 1);}, Error,  'module registered, callback has to be callable');

        raises(function(){Adaptive.registerDetectionForModule(u._name, function(){});}, Error,  'All arguments are mandatory');

        Adaptive.registerDetectionForModule(u._name, key2, function(){return true;});
        Adaptive.registerDetectionForModule(u._name, key3, function(){return false;});
        Adaptive.registerDetectionForModule(u._name, key4, function(){return 1;});

        var result = [u._name+"."+key2, u._name+"."+key4];
        deepEqual(Adaptive.detect(), result);

        raises(function(){Adaptive.registerDetectionForModule(u._name, "a.", function(){return true;});}, Error,  'detectionName should not have a dot');
        raises(function(){Adaptive.registerDetectionForModule(u._name, "a.b", function(){return true;});}, Error,  'detectionName should not have a dot');
        raises(function(){Adaptive.registerDetectionForModule(u._name, "a..b", function(){return true;});}, Error,  'detectionName should not have a dot');
        raises(function(){Adaptive.registerDetectionForModule(u._name, "a.....b", function(){return true;});}, Error,  'detectionName should not have a dot');
        raises(function(){Adaptive.registerDetectionForModule(u._name, ".....", function(){return true;});}, Error,  'detectionName should not have a dot');
        raises(function(){Adaptive.registerDetectionForModule(u._name, ".....b", function(){return true;});}, Error,  'detectionName should not have a dot');
    });


    test('Adaptive: registerDetectionForModule:bench', function(){
        expect(1);
        var moduleTimes = 1000;
        var u = new User(), userName = u._name;
        var f = new Features(), featuresName = f._name;
        var d = new Device(), deviceName = d._name;
        Adaptive.registerModule(u);
        Adaptive.registerModule(f);
        Adaptive.registerModule(d);

        for(var i=0; i < moduleTimes; i++){
            var key1 = i;
            Adaptive.registerDetectionForModule(u._name, key1, function(){return true;});
            Adaptive.registerDetectionForModule(f._name, key1, function(){return true;});
            Adaptive.registerDetectionForModule(d._name, key1, function(){return true;});
        }

        var time = benchmark(function(){
            Adaptive.detect();
        }, moduleTimes);

        var previous = "no previous";
        var average = null;
        if(typeof(Storage)!=="undefined"){
            previous = localStorage.getItem("Adaptive:registerDetectionForModule:bench:previous");
            average = localStorage.getItem("Adaptive:registerDetectionForModule:bench:average");
            localStorage.setItem("Adaptive:registerDetectionForModule:bench:previous", time);
            if(average === null || !average){
                localStorage.setItem("Adaptive:registerDetectionForModule:bench:average", time);
            }
            else{
                localStorage.setItem("Adaptive:registerDetectionForModule:bench:average", ((parseFloat(average)+parseFloat(time))/2));
            }
        }

        ok(true, 'Time of '+moduleTimes+' executions of detect with '+moduleTimes*3+' modules: '+time+'ms (previous was '+previous+' ms / average: '+average+' ms)');
    });

    test('Adaptive: adapt', function(){
        ok(Adaptive.adapt instanceof Function, 'adapt is a method');
    });

    module("Tests 'Adaptive' adaptations", {
            setup: function(){
                var d = document.createElement('div');
                d.setAttribute("id", "test-module-adapt3");
                var d2 = document.createElement('div');
                d2.setAttribute("id", "test-module-adapt4");
                d.appendChild(d2);
                document.body.appendChild(d);
                this.div3 = d;
                this.div4 = d2;
                equal(this.div3.firstChild, this.div4, 'Div4 is a child of Div');
            },
            teardown: function(){
                this.div4.parentNode.removeChild(this.div4);
                this.div3.parentNode.removeChild(this.div3);
                delete this.div3;
                delete this.div4;
            }}
    );
    test('Adaptive: registerAdaptationForModule', function(){
        expect(21);
        ok(Adaptive.registerAdaptationForModule instanceof Function, 'registerDetectionForModule is a method');
        var u = new User();
        var key1 = "1", key2 = "2", key3= "3", key4 = "4";
        var _this = this;
        Adaptive.registerModule(u);

        raises(function(){Adaptive.registerAdaptationForModule(null, null, null);}, Error,  'module not registered');
        raises(function(){Adaptive.registerAdaptationForModule(undefined, undefined, undefined);}, Error,  'module not registered');
        raises(function(){Adaptive.registerAdaptationForModule("", "", "");}, Error,  'module not registered');

        Adaptive.registerAdaptationForModule(u._name, key1, function(){});

        raises(function(){Adaptive.registerAdaptationForModule(u._name, key1, null);}, Error,  'module registered, callback has to be callable');
        raises(function(){Adaptive.registerAdaptationForModule(u._name, key1, "");}, Error,  'module registered, callback has to be callable');
        raises(function(){Adaptive.registerAdaptationForModule(u._name, key1, undefined);}, Error,  'module registered, callback has to be callable');
        raises(function(){Adaptive.registerAdaptationForModule(u._name, key1, 1);}, Error,  'module registered, callback has to be callable');

        raises(function(){Adaptive.registerAdaptationForModule(u._name, function(){});}, Error,  'All arguments are mandatory');

        raises(function(){Adaptive.registerAdaptationForModule(u._name, "a.", function(){return true;});}, Error,  'adaptationName should not have a dot');
        raises(function(){Adaptive.registerAdaptationForModule(u._name, "a.b", function(){return true;});}, Error,  'adaptationName should not have a dot');
        raises(function(){Adaptive.registerAdaptationForModule(u._name, "a..b", function(){return true;});}, Error,  'adaptationName should not have a dot');
        raises(function(){Adaptive.registerAdaptationForModule(u._name, "a.....b", function(){return true;});}, Error,  'adaptationName should not have a dot');
        raises(function(){Adaptive.registerAdaptationForModule(u._name, ".....", function(){return true;});}, Error,  'adaptationName should not have a dot');
        raises(function(){Adaptive.registerAdaptationForModule(u._name, ".....b", function(){return true;});}, Error,  'adaptationName should not have a dot');

        var cb1 = function(){
            _this.div3.style.backgroundColor = "rgb(0, 0, 0)";
        };

        var cb2 = function(){
            _this.div3.style.borderColor = "#000000";
            _this.div3.setAttribute("data-test", 12345);
        };

        var cb3 = function(){
            _this.div4 = _this.div3.removeChild(_this.div4);
            document.body.appendChild(_this.div4);
        };

        Adaptive.registerAdaptationForModule(u._name, key2, cb1);
        Adaptive.registerAdaptationForModule(u._name, key3, cb2);
        Adaptive.registerAdaptationForModule(u._name, key4, cb3);

        Adaptive.adapt(u._name+'.'+key2);
        equal(_this.div3.style.backgroundColor, "rgb(0, 0, 0)", 'Adaptation 2 well applied');

        Adaptive.adapt([u._name+'.'+key3, u._name+'.'+key4]);
        equal(_this.div3.style.borderColor, "rgb(0, 0, 0)", 'Adaptation 3.1 well applied');
        equal(_this.div3.getAttribute("data-test"), 12345, 'Adaptation 3.2 well applied');

        notEqual(_this.div3.firstChild, _this.div4, 'Adaptation 4.1 well applied');
        equal(_this.div4.parentNode, document.body, 'Adaptation 4.2 well applied');

        Adaptive.adapt('moduleNotRegistered'+'.'+key2);//Try with a module not registered
        Adaptive.adapt(u._name+'.'+'adaptationNotRegistered');//Try with an adaptation not registered
        Adaptive.adapt('weird string');
        Adaptive.adapt(undefined);
        Adaptive.adapt(null);

    });


});







